package pl.domestic.appointments.client.listener;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import org.joda.time.DateTime;

import java.util.List;

import pl.domestic.appointments.client.adapter.EventDialogSpinnerAdapter;
import pl.domestic.appointments.client.adapter.EventListViewAdapter;
import pl.domestic.appointments.client.event.EventDiagnosisUpdate;
import pl.domestic.appointments.client.event.EventEndUpdate;
import pl.domestic.appointments.client.event.EventStartUpdate;
import pl.domestic.appointments.client.event.EventUpdate;
import pl.domestic.appointments.client.model.Event;
import pl.domestic.appointments.client.model.Patient;
import pl.domestic.appointments.client.module.R;
import pl.domestic.appointments.client.helper.EventHelper;
import pl.domestic.appointments.client.user.AppData;
import pl.domestic.appointments.client.view.EventImageButton;
import pl.domestic.appointments.client.webservice.AppointmentsService;
import retrofit.RestAdapter;

/**
 * Created by Leszek_Wisniewski on 12/17/2014.
 */
public class EventOnClickListener implements View.OnClickListener {

    private static final String TAG = "NewEventOnClickListener";

    private EventDialogSpinnerAdapter eventDialogSpinnerAdapter;
    private List<Patient> patientList;
    private int timeSlot;
    private Activity activity;

    public EventOnClickListener(List<Patient> patientList, int timeSlot, Activity activity) {
        this.patientList = patientList;
        this.timeSlot = timeSlot;
        this.activity = activity;
    }

    @Override
    public void onClick(final View view) {

        final EventImageButton eventImageButton = (EventImageButton) view;
        final LayoutInflater layoutInflater = (LayoutInflater) eventImageButton.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (!eventImageButton.hasEvents()) {

            AlertDialog alertDialog = getNewEventDialog(eventImageButton, layoutInflater);
            alertDialog.show();

            Log.d(TAG, "Clicked button " + view.getId());
        } else {
            View alertDialogView = layoutInflater.inflate(R.layout.existing_events_dialog, null);

            final ListView eventListView = (ListView) alertDialogView.findViewById(R.id.eventsListView);
            final EventListViewAdapter adapter = new EventListViewAdapter(view.getContext(), eventImageButton.getEventList());
            eventListView.setAdapter(adapter);
            eventListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, final View view, int i, long l) {
                    final AlertDialog.Builder alertDialog = new AlertDialog.Builder(view.getContext());
                    alertDialog.setTitle("Co chciałbyś dokonać z tą wizytą?");
                    alertDialog.setItems(R.array.event_actions, new DialogInterface.OnClickListener() {

//                        private final RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(AppData.getUrl()).build();
//                        private final AppointmentsService appointmentsService = restAdapter.create(AppointmentsService.class);
                        private EventUpdate eventUpdateState;

                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {
                            int viewId = view.getId();
                            Event event = eventImageButton.getEventById(viewId);
                            switch(which) {
                                case 0:
                                    eventUpdateState = new EventStartUpdate(activity);
                                    break;
                                case 1:
                                    eventUpdateState = new EventEndUpdate(activity);
                                    break;
                                case 2:
                                    eventUpdateState = new EventDiagnosisUpdate(activity);
                                    break;
                                default: break;
                            }
                            eventUpdateState.execute(event);
                        }
                    });
//                    alertDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
                            // TODO: delete the event
//                            Event event = eventImageButton.getEventById(view.getId());
//                            if(event.isPartial()) {
//                                EventImageButton partial = (EventImageButton) event.getPartialView();
//                                partial.deleteEventById(view.getId());
//                                eventImageButton.deleteEventById(view.getId());
//                            } else {
//                                eventImageButton.deleteEventById(view.getId());
//                            }
//                            adapter.notifyDataSetChanged();

//                        }
//                    });
//                    alertDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            dialogInterface.dismiss();
//                        }
//                    });
                    alertDialog.create();
                    alertDialog.show();

                }
            });

            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(view.getContext());
            alertDialog.setTitle(R.string.existing_events);
            alertDialog.setView(alertDialogView);
            alertDialog.setPositiveButton(R.string.add_event, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    AlertDialog dialog = getNewEventDialog(eventImageButton, layoutInflater);
                    dialog.show();
                }
            });
            alertDialog.setNegativeButton(R.string.back, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            alertDialog.create();
            alertDialog.show();
        }
    }

    private AlertDialog getNewEventDialog(final EventImageButton eventImageButton, LayoutInflater layoutInflater) {
        View alertDialogView = (View) layoutInflater.inflate(R.layout.patient_event_dialog, null);

        eventDialogSpinnerAdapter = new EventDialogSpinnerAdapter(eventImageButton.getContext(), patientList);

        final Spinner patientSpinner = (Spinner) alertDialogView.findViewById(R.id.patientSpinner);
        patientSpinner.setAdapter(eventDialogSpinnerAdapter);

        TextView timeSlotTextView = (TextView) alertDialogView.findViewById(R.id.timeslotTextView);
        timeSlotTextView.append(String.valueOf(timeSlot) + " min");

        final TimePicker timePicker = (TimePicker) alertDialogView.findViewById(R.id.eventTimePicker);
        timePicker.setIs24HourView(true);
//        timePicker.

        final EditText editText = (EditText) alertDialogView.findViewById(R.id.editText);
//        String text = editText.getText().toString();

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(eventImageButton.getContext());
        alertDialog.setTitle(R.string.new_schedule_event_dialog_title);
        alertDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // produce new event
                int hour = timePicker.getCurrentHour() - 1;
                int minute = timePicker.getCurrentMinute();
                Patient selected = (Patient) patientSpinner.getSelectedItem();

                DateTime dateTime = new DateTime(2014, 12, 20, hour, minute);

                boolean splitEvent = EventHelper.intersects(dateTime, timeSlot);

                Event event = new Event();
                event.setPlannedStart(dateTime);
                event.setPatient(selected);
                event.setDuration(timeSlot);
                event.setDescription(editText.getText().toString());

                // TODO: set the new event
                if (!splitEvent) {
                    eventImageButton.addEvent(event);
                } else {
                    EventImageButton firstPartial = eventImageButton;
                    EventImageButton secondPartial = (EventImageButton) activity.findViewById(eventImageButton.getId() + 1);

                    event.setPartial(true);
                    Event trimmedEvent = EventHelper.trim(event);
                    Event nextEvent = EventHelper.partition(event);

                    firstPartial.addEvent(trimmedEvent);
                    secondPartial.addEvent(nextEvent);

                    trimmedEvent.setPartialView(secondPartial);
                    nextEvent.setPartialView(firstPartial);
                    secondPartial.invalidate();
                }

                eventImageButton.invalidate();
                eventDialogSpinnerAdapter.notifyDataSetChanged();
            }
        });

        alertDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        alertDialog.setView(alertDialogView);
        return alertDialog.create();
    }

}
