package pl.domestic.appointments.client.adapter;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Collection;
import java.util.List;

import pl.domestic.appointments.client.model.Event;

/**
 * Created by Leszek_Wisniewski on 12/29/2014.
 */
public class EventListViewAdapter extends BaseAdapter {

    private List<Event> eventList;
    private Context context;

    public EventListViewAdapter(Context context, Collection<Event> eventList) {
        this.eventList = (List) eventList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return eventList.size();
    }

    @Override
    public Object getItem(int position) {
        return eventList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView textView = null;

        if (convertView == null) {
            textView = new TextView(context);
        } else {
            textView = (TextView) convertView;
        }

        textView.setGravity(Gravity.CENTER);
        textView.setText(eventList.get(position).toString());
        textView.setId(eventList.get(position).getId());

        return textView;
    }
}
