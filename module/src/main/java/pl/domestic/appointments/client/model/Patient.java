package pl.domestic.appointments.client.model;

/**
 * Created by Leszek_Wisniewski on 8/25/2014.
 */
public class Patient {
    private Integer id;
    private Integer carehome;
    private String firstname;
    private String lastname;
    private String birthdate;
    private String pesel;
    private String gender;

    public Patient() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @return the patient_id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param patient_id the patient_id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the carehomeId
     */
    public Integer getCarehome() {
        return carehome;
    }

    /**
     * @param carehomeId the carehomeId to set
     */
    public void setCarehome(Integer carehome) {
        this.carehome = carehome;
    }

    /**
     * @return the first_name
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param first_name the first_name to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the last_name
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param last_name the last_name to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the birth_date
     */
    public String getBirthdate() {
        return birthdate;
    }

    /**
     * @param birth_date the birth_date to set
     */
    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    /**
     * @return the pesel
     */
    public String getPesel() {
        return pesel;
    }

    /**
     * @param pesel the pesel to set
     */
    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    /**
     * @return the sex
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param sex the sex to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return new StringBuilder().append(firstname).append(" ").append(lastname).append(" ").append(pesel).toString();
    }
}
