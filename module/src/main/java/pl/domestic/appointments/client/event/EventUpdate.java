package pl.domestic.appointments.client.event;

import android.app.ProgressDialog;
import android.content.Context;

import pl.domestic.appointments.client.model.Event;
import pl.domestic.appointments.client.module.R;
import pl.domestic.appointments.client.user.AppData;
import pl.domestic.appointments.client.webservice.AppointmentsService;
import retrofit.RestAdapter;

/**
 * Created by Leszek_Wisniewski on 7/9/2015.
 */
abstract public class EventUpdate {

    protected RestAdapter restAdapter;
    protected AppointmentsService appointmentsService;
    protected Context context;

    public EventUpdate(Context context) {
        this.context = context;
        this.restAdapter = new RestAdapter.Builder().setEndpoint(AppData.getUrl()).build();
        this.appointmentsService = restAdapter.create(AppointmentsService.class);
    }

    abstract public void execute(Event event);

    protected ProgressDialog showProgressDialog() {
        final ProgressDialog progress = new ProgressDialog(context);
        progress.setTitle(R.string.server_call);
//        progress.setMessage(R.string.server_call);
        progress.show();
        return progress;
    }

    protected void hideProgressDialog(ProgressDialog progress) {
        progress.dismiss();
    }

}
