package pl.domestic.appointments.client.event;

import android.content.Context;
import android.view.ContextThemeWrapper;

import pl.domestic.appointments.client.model.Event;

/**
 * Created by Leszek_Wisniewski on 7/9/2015.
 */
public class EventDiagnosisUpdate extends EventUpdate {

    public EventDiagnosisUpdate(Context context) {
        super(context);
    }

    @Override
    public void execute(Event event) {

    }
}
