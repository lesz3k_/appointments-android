package pl.domestic.appointments.client.module;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.squareup.timessquare.CalendarPickerView;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import pl.domestic.appointments.client.adapter.DayViewAdapter;
import pl.domestic.appointments.client.user.AppData;
import pl.domestic.appointments.client.helper.EventHelper;
import pl.domestic.appointments.client.webservice.AppointmentsService;
import pl.domestic.appointments.entity.Event;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;


public class EventCalendarActivity extends ActionBarActivity {

    private final static String TAG = "EventCalendarActivity";

    private RestAdapter restAdapter = null;
    private AppointmentsService appointmentsService = null;

    private ArrayList<pl.domestic.appointments.client.model.Event> eventList = null;
    private Map<Date, List<pl.domestic.appointments.client.model.Event>> eventMap;
    private Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_calendar);

        Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                return new Date(json.getAsJsonPrimitive().getAsLong() + 6 * 60 * 60000);
            }
        }).create();

        restAdapter = new RestAdapter.Builder().setEndpoint(AppData.getUrl()).setConverter(new GsonConverter(gson)).build();
        appointmentsService = restAdapter.create(AppointmentsService.class);

        Calendar startCalendar = Calendar.getInstance();
        startCalendar.add(Calendar.MONTH, -1);

        Calendar endCalendar = Calendar.getInstance();
        endCalendar.add(Calendar.MONTH, 1);

        final ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.show();

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 1);

        final CalendarPickerView calendarPickerView = (CalendarPickerView) findViewById(R.id.calendar_view);
        Calendar monthBack = Calendar.getInstance();
        monthBack.add(Calendar.MONTH, -1);

        Date today = new Date();

        Locale locale = new Locale("pl_PL");
        calendarPickerView.init(monthBack.getTime(), calendar.getTime(), locale);

        calendarPickerView.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
                Log.e(TAG, "Date selected: " + date.toString());

                // TODO: pass event list from eventMap to the adapter!!
                DayViewActivity.eventList = eventMap.get(date);
                i = new Intent(EventCalendarActivity.this, DayViewActivity.class);

                startActivity(i);
            }

            @Override
            public void onDateUnselected(Date date) {

            }
        });

        appointmentsService.getEventsForEmployee(1, startCalendar.getTimeInMillis(), endCalendar.getTimeInMillis(), new Callback<ArrayList<Event>>() {

            @Override
            public void success(ArrayList<Event> events, Response response) {

                EventCalendarActivity.this.eventList = new ArrayList<pl.domestic.appointments.client.model.Event>(events.size());
                for (Event e : events) {
                    pl.domestic.appointments.client.model.Event event =
                            new pl.domestic.appointments.client.model.Event();
                    event.setId(e.getId());
                    event.setPlannedStart(new DateTime(e.getStartPlanned().getTime()));
                    event.setPlannedEnd(new DateTime(e.getEndPlanned().getTime()));
                    event.setDescription(e.getAction().getAction());
                    event.setDuration((int) ((e.getEndPlanned().getTime() - e.getStartPlanned().getTime())/60000));
                    EventCalendarActivity.this.eventList.add(event);
                }

//                        EventCalendarActivity.this.i.putExtra("eventList", eventList);
                DayViewActivity.eventList = EventCalendarActivity.this.eventList;
                calendarPickerView.highlightDates(EventHelper.getHighlightableDates(EventCalendarActivity.this.eventList));
                eventMap = EventHelper.getEventsMapByDate(EventCalendarActivity.this.eventList);
                progress.dismiss();
//                Intent i = new Intent(EventCalendarActivity.this, DayViewActivity.class);
//                startActivity(i);
            }

            @Override
            public void failure(RetrofitError error) {
                progress.dismiss();
                Log.e("Could not retrieve schedule.", TAG, error);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event_calendar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
