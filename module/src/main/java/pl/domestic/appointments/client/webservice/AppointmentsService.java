package pl.domestic.appointments.client.webservice;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

import pl.domestic.appointments.client.model.Duty;
import pl.domestic.appointments.client.model.Employee;
import pl.domestic.appointments.client.model.Event;
import pl.domestic.appointments.client.model.HealthReport;
import pl.domestic.appointments.client.model.Patient;
import pl.domestic.appointments.client.model.Schedule;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by Leszek_Wisniewski on 8/20/2014.
 */
public interface AppointmentsService {

    @FormUrlEncoded
    @POST("/login")
    public void login(@Field("username") String login, @Field("password")String password, Callback<Employee> cb);

    @GET("/schedule/{id}")
    public void getSchedule(@Path("id") Integer id, Callback<List<Schedule>> cb);

    @GET("/patient/{id}")
    public void getPatient(@Path("id") Integer id, Callback<Patient> cb);

    @GET("/patients")
    public void getPatients(Callback<List<Patient>> callback);

    @GET("/timeslot/{id}")
    public void getTimeslotForEmployee(@Path("id") Integer employeeId, Callback<Integer> callback);

    @GET("/schedule/{employeeId}/{from}/{to}")
    public void getEventsForEmployee(@Path("employeeId") Integer employeeId, @Path("from") Long from, @Path("to") Long to,
                                     Callback<ArrayList<pl.domestic.appointments.entity.Event>> callback);

    @FormUrlEncoded
    @POST("/event/{eventId}/diagnosis/{diagnosisId}")
    public void addDiagnosis(@Path("eventId") Integer eventId, @Path("code") String icd10code);

    @FormUrlEncoded
    @POST("/event/end")
    public void addEventEnd(@Field("eventId") Integer eventId, @Field("end") Long end, Callback<Integer> cb);

    @FormUrlEncoded
    @POST("/event/start")
    public void addEventStart(@Field("eventId") Integer eventId, @Field("start") Long start, Callback<Integer> cb);

}
