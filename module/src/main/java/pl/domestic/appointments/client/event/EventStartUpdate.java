package pl.domestic.appointments.client.event;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.Toast;

import org.joda.time.DateTime;

import java.util.Date;

import pl.domestic.appointments.client.model.Event;
import pl.domestic.appointments.client.module.R;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Leszek_Wisniewski on 7/9/2015.
 */
public class EventStartUpdate extends EventUpdate {

    private static final String TAG = "EventStartUpdate";

    public EventStartUpdate(Context context) {
        super(context);
    }

    @Override
    public void execute(Event event) {
        final ProgressDialog progressDialog = showProgressDialog();
        if(event.getActualStart() != null) {
            showAlreadyExistsAlertDialog();
        } else {
            Long timestamp = new Date().getTime();

            event.setActualStart(new DateTime(timestamp));

            appointmentsService.addEventStart(event.getId(), timestamp, new Callback<Integer>() {
                @Override
                public void success(Integer integer, Response response) {
                    if(integer.intValue() == 1) {
                        hideProgressDialog(progressDialog);
                        Toast toast = Toast.makeText(context, R.string.start_successfully_added, Toast.LENGTH_LONG);
                        toast.show();
                    }

                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("Error while requesting server", TAG, error);
                    hideProgressDialog(progressDialog);
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setTitle(R.string.server_problem);
                    alertDialog.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                }
            });
        }


    }

    private void showAlreadyExistsAlertDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(R.string.start_already_added);
        alertDialog.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
    }
}
