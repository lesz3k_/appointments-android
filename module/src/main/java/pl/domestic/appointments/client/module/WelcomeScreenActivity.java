package pl.domestic.appointments.client.module;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import pl.domestic.appointments.client.model.Employee;
import pl.domestic.appointments.client.user.AppData;
import pl.domestic.appointments.client.webservice.AppointmentsService;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

public class WelcomeScreenActivity extends ActionBarActivity {

    private static final String TAG = "WelcomeScreenActivity";

    private static String loginText = null;
    private static String passwordText = null;

    private RestAdapter restAdapter = null;
    private AppointmentsService appointmentsService = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        restAdapter = new RestAdapter.Builder().setEndpoint(AppData.getUrl()).build();
        appointmentsService = restAdapter.create(AppointmentsService.class);

        final EditText loginEditText = (EditText) findViewById(R.id.loginEditText);
        loginEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                loginText = loginEditText.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        final EditText passwordEditText = (EditText) findViewById(R.id.pwEditText);
        passwordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                passwordText = passwordEditText.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        final Button loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                appointmentsService.login(loginText, passwordText, new Callback<Employee>() {
                    @Override
                    public void success(Employee employee, Response response) {
                        CookieManager cookieManager = CookieManager.getInstance();
                        AppData appData = AppData.getInstance();
                        String url = appData.getUrl();

                            List<Header> headers = response.getHeaders();
                            for(Header header : headers) {
                                String headerName = header.getName();
                                if(headerName != null && headerName.equals("Set-Cookie")) {
                                    String val = header.getValue();
                                    cookieManager.setCookie(url, val);
                                }
                            }

                        Log.d("Udane logowanie [{}]. ", response.getHeaders().toString());
                        TextView tv = (TextView) findViewById(R.id.successTextView);
                        String displayName = new StringBuilder().append("Witaj ").append(employee.getFirstname()).append(" ").append(employee.getLastname()).toString();
                        appData.setEmployee(employee);
                        tv.setText(displayName);

                        Intent i = new Intent(WelcomeScreenActivity.this, UserMenuActivity.class);
                        startActivity(i);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("Error occurred on user login.", TAG, error);
                        TextView tv = (TextView) findViewById(R.id.successTextView);
                        tv.setText("Nieudane logowanie!");
                    }
                });
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.welcome_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}


