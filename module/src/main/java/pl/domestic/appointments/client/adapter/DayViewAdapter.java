package pl.domestic.appointments.client.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import pl.domestic.appointments.client.listener.EventOnClickListener;
import pl.domestic.appointments.client.model.Event;
import pl.domestic.appointments.client.model.Patient;
import pl.domestic.appointments.client.module.R;
import pl.domestic.appointments.client.view.EventImageButton;

/**
 * Created by Leszek_Wisniewski on 1/12/2015.
 */
public class DayViewAdapter extends BaseAdapter {

    private Activity activity;
    private List<Event> eventList;
    private int[] displayableHours;

    public DayViewAdapter(Activity activity, int[] displayableHours, List<Event> eventList) {
        this.activity = activity;
        this.displayableHours = displayableHours;
        this.eventList = eventList;
//        for(int i = 0 ; i < displayableHours.length; i++) {
//            displayableHours[i] = i + displayableHours.length;
//        }
    }

    @Override
    public int getCount() {
        return displayableHours.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return displayableHours[position];
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        EventHolder eventHolder = null;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = layoutInflater.inflate(R.layout.schedule_list_item, parent, false);
            eventHolder = new EventHolder();

            GradientDrawable gd = new GradientDrawable();
            gd.setColor(0x00000000);
            gd.setCornerRadius(3);
            gd.setStroke(1, 0xFF000000);

            eventHolder.hourTextView = (TextView) convertView.findViewById(R.id.hourTextView);
            eventHolder.hourTextView.setBackground(gd);

            eventHolder.eventImageButton = (EventImageButton) convertView.findViewById(R.id.eventImageButton);
            eventHolder.eventImageButton.setBackground(gd);
            eventHolder.eventImageButton.setId(View.generateViewId());
            eventHolder.eventImageButton.renderEvents(displayableHours[position], eventList);
            eventHolder.eventImageButton.setOnClickListener(new EventOnClickListener(getPatients(), 20,activity));

            convertView.setTag(eventHolder);

        } else {
            eventHolder = (EventHolder) convertView.getTag();
        }

        eventHolder.hourTextView.setText(String.valueOf(displayableHours[position]));

        return convertView;
    }

    private List<Patient> getPatients() {

        Patient patient1 = new Patient();
        patient1.setBirthdate("27-06-1988");
        patient1.setPesel("88062705891");
        patient1.setFirstname("Adam");
        patient1.setLastname("Orłowski");

        Patient patient2 = new Patient();
        patient2.setBirthdate("11-03-1980");
        patient2.setPesel("80031169312");
        patient2.setFirstname("Damian");
        patient2.setLastname("Dzierliński");

        List<Patient> patients = new ArrayList<Patient>(2);
        patients.add(patient1);
        patients.add(patient2);

        return patients;
    }

    // mock data

    private static class EventHolder {

        TextView hourTextView = null;
        EventImageButton eventImageButton = null;

    }


    private void getHoursFromEvents() {
        Collections.sort(eventList, new Comparator<Event>() {
            @Override
            public int compare(Event e1, Event e2) {
                DateTime time1 = e1.getPlannedStart();
                DateTime time2 = e2.getPlannedStart();
                if (time1.isAfter(time2)) {
                    return -1;
                } else {
                    return 1;
                }
            }
        });

        DateTime first = eventList.get(0).getPlannedStart();
        DateTime last = eventList.get(eventList.size() - 1).getPlannedStart();

        int firstHour = first.getHourOfDay();
        int lastHour = last.getHourOfDay();

        displayableHours = new int[lastHour - firstHour];
        for(int i = 0; i < displayableHours.length; i++) {
            displayableHours[i] = firstHour++;
        }
    }

//    private List<Event> getEventList() {
//        List<Event> eventList = new ArrayList<Event>(1);
//
//        DateTime builder = new DateTime();
//        DateTime start = builder.withTime(5, 0, 0, 0);
//
//        DateTime end = builder.withTime(5, 10, 0, 0);
//
//        Event event = new Event();
//        event.setDescription("Test event");
//        event.setPlannedStart(start);
//        event.setPlannedEnd(end);
//        event.setDuration(10);
//
//        eventList.add(event);
//
//        return eventList;
//    }

}
