package pl.domestic.appointments.client.helper;

import android.graphics.RectF;

import org.joda.time.DateTime;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import pl.domestic.appointments.client.EventDateTimeComparator;
import pl.domestic.appointments.client.model.Event;

/**
 * Created by Leszek_Wisniewski on 12/20/2014.
 * Helper class to convert event business data to onDraw
 * method parameters for graphic representation on canvas.
 */
public class EventHelper {

    public static int PADDING_X = 15;
    public static int MINUTES_IN_HOUR = 60;

//    screen orientation?
//    public static int get

    public static RectF getEventRectF(Event event, int height, int width) {
        float scaleY = getScaleY(height);
        float startX = PADDING_X;
        float startY = scaleY * event.getPlannedStart().getMinuteOfHour();
        float endX = width - PADDING_X;
        float endY = startY + event.getDuration() * scaleY;
        return new RectF(startX, startY, endX, endY);
    }

    public static float getStartY(int height, DateTime time) {
        float scaleY = getScaleY(height);
        float timeOnScale = time.getMinuteOfHour();

        return timeOnScale * scaleY;
    }

    public static float getScaleY(int height) {
        return height / 60.0f;
    }

    public static float getEndY(float startY, int duration) {
        return startY + duration;
    }

    public static boolean isPartial(Event event) {
        return event.getDuration() + event.getPlannedStart().getMinuteOfHour() > MINUTES_IN_HOUR;
    }

    public static Event partition(Event event) {

        int diff = event.getDuration() + event.getPlannedStart().getMinuteOfHour() - MINUTES_IN_HOUR;
        Event nextEvent = event.clone();
        nextEvent.setDuration(diff);

        int nextHour = event.getPlannedStart().getHourOfDay() + 1;
        DateTime nextDateTime = new DateTime(2014, 12, 20, nextHour, 0);
        nextEvent.setPlannedStart(nextDateTime);

        return nextEvent;
    }

    public static Event trim(Event event) {
        int leftover = MINUTES_IN_HOUR - event.getPlannedStart().getMinuteOfHour();

        Event trimmed = event.clone();
        trimmed.setDuration(leftover);
        return trimmed;
    }

    public static int generateId(int start) {
        AtomicInteger atomicInteger = new AtomicInteger(start);
        int id = atomicInteger.getAndIncrement();
        return id;
//        return new StringBuilder()
//                .append(event.getPatient())
//                .append(event.getPlannedStart().getMillisOfDay())
//                .toString();
    }

    public static String getDutyId(DateTime date) {

        return new StringBuilder()
                .append(date.getYear())
                .append(date.getMonthOfYear())
                .append(date.getDayOfMonth()).toString();
    }

    public static boolean intersects(DateTime dateTime, int timeSlot) {
        int minutes = dateTime.getMinuteOfHour();
        int relativeTimeLength = minutes + timeSlot;
        return relativeTimeLength > MINUTES_IN_HOUR;
    }

    public static DateTime getEndOfDayDateTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return new DateTime(cal.getTime());
    }

    public static Collection<Date> getHighlightableDates(Collection<Event> events) {
        Set<Date> dateTimeSet = new HashSet<Date>(events.size());
        for (Event e : events) {
            dateTimeSet.add(new Date(e.getPlannedStart().getMillis()));
        }
        return dateTimeSet;
    }

    public static Map<Date, List<Event>> getEventsMapByDate(List<Event> eventList) {

        Map<Date, List<Event>> eventMap = new HashMap<Date, List<Event>>();

        for (Event e : eventList) {
            Date dateWoTime = e.getPlannedStart().toLocalDate().toDate();
            if(!eventMap.containsKey(dateWoTime)) {
                eventMap.put(dateWoTime, new LinkedList<Event>());
            }
            eventMap.get(dateWoTime).add(e);
        }

        return eventMap;
    }
}
