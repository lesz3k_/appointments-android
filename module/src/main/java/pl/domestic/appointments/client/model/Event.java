package pl.domestic.appointments.client.model;

import android.graphics.Paint;
import android.view.View;

import org.joda.time.DateTime;

/**
 * Created by Leszek_Wisniewski on 12/20/2014.
 */
public class Event {

    private int id;
    private int duration;
    private DateTime plannedStart;
    private DateTime plannedEnd;
    private DateTime actualStart;
    private DateTime actualEnd;
    private Patient patient;
    private String description;
    private Paint paint;
    private boolean partial;
    private View partialView;


    public Event() {
    }

    private Event(Event original) {
        this.duration = original.getDuration();
        this.plannedStart = original.getPlannedStart();
        this.patient = original.getPatient();
        this.description = original.getDescription();
        this.partial = original.isPartial();
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getDuration() {
        return duration;
    }

    public void setPlannedStart(DateTime plannedStart) {
        this.plannedStart = plannedStart;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public DateTime getPlannedStart() {
        return plannedStart;
    }

    public Patient getPatient() {
        return patient;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
//
//    public Paint getPaint() {
//        return paint;
//    }
//
//    public void setPaint(Paint paint) {
//        this.paint = paint;
//    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isPartial() {
        return partial;
    }

    public void setPartial(boolean partial) {
        this.partial = partial;
    }

    public View getPartialView() {
        return partialView;
    }

    public void setPartialView(View partialView) {
        this.partialView = partialView;
    }

    public DateTime getPlannedEnd() {
        return plannedEnd;
    }

    public void setPlannedEnd(DateTime plannedEnd) {
        this.plannedEnd = plannedEnd;
    }

    public DateTime getActualStart() {
        return actualStart;
    }

    public void setActualStart(DateTime actualStart) {
        this.actualStart = actualStart;
    }

    public DateTime getActualEnd() {
        return actualEnd;
    }

    public void setActualEnd(DateTime actualEnd) {
        this.actualEnd = actualEnd;
    }

    public Paint getPaint() {
        return paint;
    }

    public void setPaint(Paint paint) {
        this.paint = paint;
    }


    @Override
    public Event clone() {
        return new Event(this);
    }

    @Override
    public String toString() {
        return new StringBuilder().append(patient).append(" - ").append(description).toString();
    }
}
