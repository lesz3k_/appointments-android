package pl.domestic.appointments.client.model;

import org.joda.time.DateTime;

/**
 * Created by Leszek_Wisniewski on 1/12/2015.
 */
public class Duty {

    private String id;
    private DateTime startTime;
    private DateTime endTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(DateTime startTime) {
        this.startTime = startTime;
    }

    public DateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(DateTime endTime) {
        this.endTime = endTime;
    }

}
