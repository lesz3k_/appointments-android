package pl.domestic.appointments.client;

import org.joda.time.DateTime;

import java.util.Comparator;

import pl.domestic.appointments.client.model.Event;

/**
 * Created by Leszek_Wisniewski on 7/1/2015.
 */
public class EventDateTimeComparator<E> implements Comparator<Event> {

    @Override
    public int compare(Event e1, Event e2) {
        DateTime time1 = e1.getPlannedStart();
        DateTime time2 = e2.getPlannedStart();
        if (time1.isAfter(time2)) {
            return -1;
        } else {
            return 1;
        }
    }
}
