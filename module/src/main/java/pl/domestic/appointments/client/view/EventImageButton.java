package pl.domestic.appointments.client.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import pl.domestic.appointments.client.helper.EventHelper;
import pl.domestic.appointments.client.model.Event;

/**
 * Created by Leszek_Wisniewski on 12/20/2014.
 */
public class EventImageButton extends View {

    private final static int eventColor = Color.rgb(194, 241, 245);

    private Context context;
    private List<Event> eventList = new LinkedList<Event>();

    public EventImageButton(Context context) {
        super(context);
    }

    public EventImageButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public EventImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        final float padding = 15;

        Paint paint = new Paint();
        paint.setColor(eventColor);

        int width = getMeasuredWidth();
        int height = getMeasuredHeight();

        for (Event e : eventList) {
            RectF eventRectF = EventHelper.getEventRectF(e, height, width);
            canvas.drawRoundRect(eventRectF, 3.5f, 3.5f, paint);
        }
    }

    public void addEvent(Event event) {
        eventList.add(event);
    }

    public boolean hasEvents() {
        return eventList.size() > 0;
    }

    public List<Event> getEventList() {
        return eventList;
    }

    public Event getEventById(int id) {
        Iterator<Event> it = eventList.iterator();
        while(it.hasNext()) {
            Event e = it.next();
            if(e.getId() == id) {
                return e;
            }
        }
        return null;
    }

    public void deleteEventById(int id) {
        Iterator<Event> it = eventList.iterator();
        while (it.hasNext()) {
            Event e = it.next();
            if (e.getId() == id) {
                it.remove();
                invalidate();
            }
        }

    }

    public void renderEvents(int displayableHour, List<Event> eventList) {
        // TODO: image placement logic...

        for(Event e : eventList) {
            int startHour = e.getPlannedStart().getHourOfDay();
            int endHour = e.getPlannedEnd().getHourOfDay();
            if(startHour == displayableHour && endHour == displayableHour) {
                e.setPartial(false);
                this.eventList.add(e);
            } else if(startHour == displayableHour && endHour == (displayableHour + 1)) {
                Event partialEvent = EventHelper.trim(e);
                this.eventList.add(partialEvent);
            } else if(startHour + 1 == displayableHour && endHour == displayableHour) {
                Event partialEvent = EventHelper.partition(e);
                this.eventList.add(partialEvent);
            }
        }

        this.invalidate();
    }
}
