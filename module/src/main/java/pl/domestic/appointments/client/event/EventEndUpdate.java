package pl.domestic.appointments.client.event;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

import java.util.Date;

import pl.domestic.appointments.client.model.Event;
import pl.domestic.appointments.client.module.R;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Leszek_Wisniewski on 7/9/2015.
 */
public class EventEndUpdate extends EventUpdate {

    public EventEndUpdate(Context context) {
        super(context);
    }

   @Override
   public void execute(Event event) {
        final ProgressDialog progressDialog = showProgressDialog();
        if(event.getActualEnd() != null) {
            showAlreadyExistsAlertDialog();
        }

        appointmentsService.addEventStart(event.getId(), new Date().getTime(), new Callback<Integer>() {
            @Override
            public void success(Integer integer, Response response) {
                if(integer.intValue() == 1) {
                    hideProgressDialog(progressDialog);
                    Toast toast = Toast.makeText(context, R.string.start_successfully_added, Toast.LENGTH_LONG);
                    toast.show();
                } else {
                    hideProgressDialog(progressDialog);
                    showAlreadyExistsAlertDialog();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                hideProgressDialog(progressDialog);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle(R.string.server_problem);
                alertDialog.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
            }
        });
    }

    private void showAlreadyExistsAlertDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(R.string.end_already_added);
        alertDialog.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
    }
}
