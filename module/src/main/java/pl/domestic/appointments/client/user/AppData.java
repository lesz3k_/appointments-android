package pl.domestic.appointments.client.user;

import android.text.format.DateUtils;

import pl.domestic.appointments.client.model.Employee;

/**
 * Created by Leszek_Wisniewski on 8/28/2014.
 */
public class AppData {

    private static AppData appData = null;
    private static final String url = "http://172.20.81.194:8080/appointments-frontend/mobile";
    private static final int dateFlags =  (DateUtils.FORMAT_SHOW_DATE |
            DateUtils.FORMAT_SHOW_TIME |
            DateUtils.FORMAT_NUMERIC_DATE);
    private Employee employee;

    private AppData() {
    }

    public static AppData getInstance() {
        if(appData == null) {
            appData = new AppData();
        }
        return appData;
    }

    public static int getDateFlags() {
        return dateFlags;
    }

    public static String getUrl() {
        return url;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }

    // called when logout is performed
    public static void destroy() {
        appData = null;
    }

}
