package pl.domestic.appointments.client.model;

/**
 * Created by Leszek_Wisniewski on 8/25/2014.
 */
public class Schedule {

    private Integer id;
    private Patient patient;
    private Action action;
    private Employee employee;
    private Long date;

    public Schedule() {
        // TODO Auto-generated constructor stub
    }

    public Schedule(Employee employee, Patient patient, Action action,
                    Long date) {
        this.employee = employee;
        this.patient = patient;
        this.action = action;
        this.date = date;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the patient
     */
    public Patient getPatient() {
        return patient;
    }

    /**
     * @param patient
     *            the patient to set
     */
    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    /**
     * @return the action
     */
    public Action getAction() {
        return action;
    }

    /**
     * @param action
     *            the action to set
     */
    public void setAction(Action action) {
        this.action = action;
    }

    /**
     * @return the employee
     */
    public Employee getEmployee() {
        return employee;
    }

    /**
     * @param employee
     *            the employee to set
     */
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    /**
     * @return the date
     */
    public Long getDate() {
        return date;
    }

    /**
     * @param date
     *            the date to set
     */
    public void setDate(Long date) {
        this.date = date;
    }

}