package pl.domestic.appointments.client.listener;

import android.graphics.Matrix;
import android.view.ScaleGestureDetector;
import android.view.View;

/**
 * Created by Leszek_Wisniewski on 12/24/2014.
 */
public class PinchZoomListener implements ScaleGestureDetector.OnScaleGestureListener {

    private Matrix matrix;
    private float lastFocusX;
    private float lastFocusY;

    private View view;

    public PinchZoomListener(View view) {
        this.view = view;
    }

    @Override
    public boolean onScaleBegin(ScaleGestureDetector detector) {
        lastFocusX = detector.getFocusX();
        lastFocusY = detector.getFocusY();
        return true;
    }

    @Override
    public boolean onScale(ScaleGestureDetector detector) {
        Matrix transformationMatrix = new Matrix();
        float focusX = detector.getFocusX();
        float focusY = detector.getFocusY();

        //Zoom focus is where the fingers are centered,
        transformationMatrix.postTranslate(-focusX, -focusY);

        transformationMatrix.postScale(detector.getScaleFactor(), detector.getScaleFactor());

        float focusShiftX = focusX - lastFocusX;
        float focusShiftY = focusY - lastFocusY;
        transformationMatrix.postTranslate(focusX + focusShiftX, focusY + focusShiftY);
        matrix.postConcat(transformationMatrix);
        lastFocusX = focusX;
        lastFocusY = focusY;
        view.invalidate();
        return true;
    }

    @Override
    public void onScaleEnd(ScaleGestureDetector detector) {

    }
}
