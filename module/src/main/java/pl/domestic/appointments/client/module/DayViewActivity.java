package pl.domestic.appointments.client.module;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import pl.domestic.appointments.client.adapter.DayViewAdapter;
import pl.domestic.appointments.client.model.Event;
import pl.domestic.appointments.client.model.Patient;
import pl.domestic.appointments.client.user.AppData;
import pl.domestic.appointments.client.webservice.AppointmentsService;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class DayViewActivity extends ListActivity {

    private static final String TAG = "DayViewActivity";
    private static int NUM_OF_HOURS = 24;

//    private TableLayout tableLayout;


    private RestAdapter restAdapter = null;
    private AppointmentsService appointmentsService = null;

    private List<Patient> patientList;
    public static List<Event> eventList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.layout_schedule_list_view);


        restAdapter = new RestAdapter.Builder().setEndpoint(AppData.getUrl()).build();
        appointmentsService = restAdapter.create(AppointmentsService.class);

        appointmentsService.getPatients(new Callback<List<Patient>>() {
            @Override
            public void success(List<Patient> patients, Response response) {
                DayViewActivity.this.patientList = patients;
            }

            @Override
            public void failure(RetrofitError error) {
                return;
            }
        });

//        Intent startingIntent = getIntent();
//
//        DayViewAdapter adapter = new DayViewAdapter(this, eventList);
//        adapter.notifyDataSetChanged();
//        setListAdapter(adapter);

        Intent startingIntent = getIntent();
        int start = startingIntent.getIntExtra("startHour", 0);
        int end = startingIntent.getIntExtra("endHour", 12);

        int[] hours = new int[end-start];

        int cnt = 0;
        for(int i = start; i < end; i++) {
            hours[cnt++] = i;
        }

        DayViewAdapter adapter = new DayViewAdapter(this, hours, eventList);
        setListAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.calendar_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    // mock data
    private List<Patient> getPatients() {



        Patient patient1 = new Patient();
        patient1.setBirthdate("27-06-1988");
        patient1.setPesel("88062705891");
        patient1.setFirstname("Adam");
        patient1.setLastname("Orłowski");

        Patient patient2 = new Patient();
        patient2.setBirthdate("11-03-1980");
        patient2.setPesel("80031169312");
        patient2.setFirstname("Damian");
        patient2.setLastname("Dzierliński");

        List<Patient> patients = new ArrayList<Patient>(2);
        patients.add(patient1);
        patients.add(patient2);

        return patients;
    }

    // mock data
    private int getTimeslot() {
        return 20;
    }

}
