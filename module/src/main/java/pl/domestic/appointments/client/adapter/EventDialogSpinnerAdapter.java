package pl.domestic.appointments.client.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import pl.domestic.appointments.client.model.Patient;

/**
 * Created by Leszek_Wisniewski on 12/19/2014.
 */
public class EventDialogSpinnerAdapter extends BaseAdapter {

    private Context context;
    private List<Patient> patientList;

    public EventDialogSpinnerAdapter(Context context, List<Patient> patientList) {
        this.context = context;
        this.patientList = patientList;
    }

    @Override
    public int getCount() {
        return patientList.size();
    }

    @Override
    public Object getItem(int i) {
        return patientList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView textView;

        if (view == null) {
            textView = new TextView(context);
        } else {
            textView = (TextView) view;
        }

        textView.setGravity(Gravity.CENTER);
        textView.setText(patientList.get(i).toString());

        return textView;
    }

}
